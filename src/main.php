<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Films</title>
  </head>
  <body>
    
  <?php
  date_default_timezone_set('Europe/Paris');

  try{
    $file_db = new PDO('sqlite:db/contacts.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $file_db->exec("DROP TABLE contacts");
    $file_db->exec("CREATE TABLE contacts(
                    id INTEGER PRIMARY KEY,
                    nom TEXT,
                    prenom TEXT,
                    date TEXT)");

    $contacts = array(
                array('nom' => 'De Guillemets',
                      'prenom' => 'Virgule',
                      'date' => '04/01/99'),
                array('nom' => 'Talon',
                      'prenom' => 'Achille',
                      'date' => '02/01/98'),
                array('nom' => 'Higgs',
                      'prenom' => 'Peter',
                      'date' => '04/10/97') );

    $insert = "INSERT INTO contacts(nom,prenom,date) VALUES (:nom,:prenom, :date)";

    $stmt = $file_db->prepare($insert);

    $stmt->bindParam(':nom',$nom);
    $stmt->bindParam(':prenom',$prenom);
    $stmt->bindParam(':date',$time);

    foreach($contacts as $c){
      $nom = $c['nom'];
      $prenom = $c['prenom'];
      $time = $c['date'];

      $stmt->execute();
    }

    echo "contacts dans Base!";

    $result = $file_db->query('SELECT * FROM contacts');

    echo "<ul>";
    foreach($result as $m){
      echo "<li>".$m['prenom'].' '.$m['nom'].' '.$m['date']."</li>"."<br>";

    }
    echo"</ul>";



    $file_db = null;
  }

  catch(PDOException $e){

      echo $e->getMessage();
  }







    ?>
  </body>
</html>
