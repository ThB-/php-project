<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <meta charset="utf-8">
    <title></title>
        <link rel="stylesheet" type="text/css" href="style_recherche.css">
  </head>
  <header>
    <img src="../images/cine-mag.jpg">
    <h2>Le site premier aux Box-Office</h2>
    <div class="recherche_p">
      <form action="rechercher.php" method="GET">
        <input id="search" name="texte_recherche" type="text" placeholder="Nom de film" />
        <input id="search-btn" type="submit" value="Rechercher" />
      </form>
    </div>
  </header>
  <body>
    <ul id="navigation">
      <li><a href="presentation.php" title="item1">Tout les films</a></li>
      <li><a href="rechercher.html">Affiner recherche</a></li>
      <li><a href="gerer_film.html">Gérer film</a></li>
      <li><a href="bande_annonce.html">Bande Annonce</a></li>
      <li><a href="#" title="item6">Avis</a></li>
    </ul>
    <?php
    date_default_timezone_set('Europe/Paris');


    $name = $_GET['texte_recherche'];


    try{
      $file_db = new PDO('sqlite:db/films.db');
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $requete = "SELECT * from FILM where Titre = :nom";

      $recherche = $file_db -> prepare($requete);

      $recherche -> bindParam(':nom',$name);


      $recherche->execute();

      $data = $recherche->fetchAll();

      if(sizeof($data) > 0){
          echo "<div class='row'>";
        foreach($data as $film){
          echo "<div class='card'>";
          echo "<div class='card-image'>";
          echo "<img src = '../images/$film[Id].jpeg'>";
          echo "<span class='card-title'>$film[Titre]</span>";
          echo "<a class='btn-floating halfway-fab waves-effect waves-light red'><i class='material-icons'>add_circle</i></a>";
          echo "</div>";
          echo "<div class='card-content'>";
          echo "<p>Réalisé par: $film[Realisateur] en $film[Année]</p>";
          echo "</div>";
          echo "</div>";

        }

        echo "</div>";

      }
      else{
        echo "Le film $name n'existe pas.";
      }






      $file_db = null;

  }
    catch(PDOException $e){

        echo $e->getMessage();
    }

    ?>

  </body>
  <footer>
    <div>
      <p>Retrouvez nous aussi sur : </p>
      <ul>
        <li><a href="http://www.allocine.fr/%22%3E"><img src="../images/allociné.png"></a></li>
        <li><a href="https://fr-fr.facebook.com/allocine/%22%3E"><img src="../images/facebook.png"></a></li>
        <li><a href="https://www.instagram.com/allocine/%22%3E"><img src="../images/instagram.png"></a></li>
        <li><a href="https://twitter.com/allocine?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor%22%3E"><img src="../images/twitter-1.png"></a></li>
      </ul>
    </div>
  </footer>
</html>
