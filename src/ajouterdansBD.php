
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
        <link rel="stylesheet" type="text/css" href="presentation.css">
  </head>
  <header>
    <img src="../images/cine-mag.jpg">
    <h2>Le site premier aux Box-Office</h2>
    <div class="recherche_p">
      <form action="rechercher.php" method="GET">
        <input id="search" name="texte_recherche" type="text" placeholder="Nom de film" />
        <input id="search-btn" type="submit" value="Rechercher" />
      </form>
    </div>
  </header>
  <body>
    <ul id="navigation">
      <li><a href="presentation.php" title="item1">Tout les films</a></li>
      <li><a href="rechercher.html">Affiner recherche</a></li>
      <li><a href="gerer_film.html">Gérer film</a></li>
      <li><a href="bande_annonce.html">Bande Annonce</a></li>
      <li><a href="#" title="item6">Avis</a></li>
    </ul>
    <?php
    date_default_timezone_set('Europe/Paris');


    $name = $_GET['name'];
    $realisateur = $_GET['réalisateur'];
    $date = $_GET['date'];
    $genre = $_GET['genre'];

    try{
      $file_db = new PDO('sqlite:db/films.db');
      $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $film_a_ajouter = "INSERT INTO film(titre,realisateur,Année,genre) VALUES (:name,:realisateur,:date,:genre)";


      $stmt = $file_db->prepare($film_a_ajouter);

      $stmt -> bindParam(':name',$name);
      $stmt -> bindParam(':realisateur',$realisateur);
      $stmt -> bindParam(':date',$date);
      $stmt -> bindParam(':genre',$genre);

      $stmt->execute();

      echo "Insertion réussie";

      $file_db = null;

}
    catch(PDOException $e){

        echo $e->getMessage();
    }

    ?>

  </body>
  <footer>
    <div>
      <p>Retrouvez nous aussi sur : </p>
      <ul>
        <li><a href="http://www.allocine.fr/%22%3E"><img src="../images/allociné.png"></a></li>
        <li><a href="https://fr-fr.facebook.com/allocine/%22%3E"><img src="../images/facebook.png"></a></li>
        <li><a href="https://www.instagram.com/allocine/%22%3E"><img src="../images/instagram.png"></a></li>
        <li><a href="https://twitter.com/allocine?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor%22%3E"><img src="../images/twitter-1.png"></a></li>
      </ul>
    </div>
  </footer>
</html>
