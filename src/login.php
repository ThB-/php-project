<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
        <link rel="stylesheet" type="text/css" href="presentation.css">
  </head>
  <header>
    <img src="../images/cine-mag.jpg">
    <h2>Le site premier aux Box-Office</h2>
    <div class="recherche_p">
      <form action="/search" id="searchthis" method="get">
        <input id="search" name="q" type="text" placeholder="Rechercher" />
        <input id="search-btn" type="submit" value="Rechercher" />
      </form>
    </div>
  </header>
    <body>
        <div id="container">
            <!-- zone de connexion -->

            <form action="verification.php" method="POST">
                <h1>Connexion</h1>

                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>

                <input type="submit" id='submit' value='LOGIN' >
                <?php
                if(isset($_GET['erreur'])){
                    $err = $_GET['erreur'];
                    if($err==1 || $err==2)
                        echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                }
                ?>
            </form>
        </div>
    </body>
    <footer>
      <div>
        <p>Retrouvez nous aussi sur : </p>
        <ul>
          <li><a href="http://www.allocine.fr/%22%3E"><img src="../images/allociné.png"></a></li>
          <li><a href="https://fr-fr.facebook.com/allocine/%22%3E"><img src="../images/facebook.png"></a></li>
          <li><a href="https://www.instagram.com/allocine/%22%3E"><img src="../images/instagram.png"></a></li>
          <li><a href="https://twitter.com/allocine?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor%22%3E"><img src="../images/twitter-1.png"></a></li>
        </ul>
      </div>
    </footer>
</html>
